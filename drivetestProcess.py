    # DriveTest Map is a free tool that creates kml files from GMON generated data
    # Copyright (C) 2019  Christian Jones
    #
    # This file is part of DriveTest Map.
    #
    # DriveTest Map is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.
    #
    # DriveTest Map is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.
    #
    # You should have received a copy of the GNU General Public License
    # along with DriveTest Map.  If not, see <https://www.gnu.org/licenses/>.

import random
import csv
import argparse
import json
from xml.dom import minidom
from colour import Color


def transcell(infile):
    '''
    Opens csv file composed of "CID, Friendly Name" and returns a dictionary.
    '''
    try:
        with open(infile, mode='r') as infile:
            reader = csv.reader(infile)
            mydict = {rows[0]:rows[1] for rows in reader}
    except FileNotFoundError:
        mydict = {}
    return mydict


def readPrefs():
    with open('prefs.json', 'r') as infile:
        prefDict = json.load(infile)
    return prefDict


def fmtcols(mylist, cols):
    '''
    Modified and taken from 'masat' ref:
    http://stackoverflow.com/questions/171662/formatting-a-list-of-text-into-columns
    This function simply allows the Unique cells seen dialog to show the cells
    in two columns - nicely formatted.  Takes a list and the number of columns to
    display
    '''
    maxwidth = max(map(lambda x: len(x), mylist))
    justifyList = list(map(lambda x: x.ljust(maxwidth,' '), mylist))
    lines = ("\t".join(justifyList[i:i+cols]) for i in range(0,len(justifyList),cols))
    return(str('\n'.join(lines)))


def analyze(infile):
    '''
    Looks through a gmon file and summarizes what it sees.  Useful when you have
    many drivetests and they are poorly named.
    infile - the gmon file that is being processed.
    Returns a string summarizing the gmon generated csv file.
    '''

    prefDict = readPrefs()
    trans_dict = transcell(prefDict['CellDpath'])
    myl2 = []
    var = infile.split("/")
    var2 = var[-1]
    data = open(infile)
    read = csv.reader(data, delimiter = ';')
    bad = 0
    good = 0
    medium = 0
    terrible = 0
    HSPA = 0
    HSPAP = 0
    HSDPA = 0
    UMTS = 0
    LTE = 0
    NA = 0
    EDGE = 0
    GPRS = 0
    UNKNOWN = 0
    messagetext2 =""
    opstring = ''
    next(read)
    for row in read:
        if row[1] in trans_dict:
            trans = trans_dict[str(row[1])]
        else:
            trans = "Not in dictionary"
        if row[11] == "HSPA+":
            HSPAP = HSPAP + 1
        elif row[11] == "HSPA":
            HSPA = HSPA + 1
        elif row[11] == "HSDPA":
            HSDPA = HSDPA + 1
        elif row[11] == "n/a":
            NA = NA + 1
        elif row[11] == "EDGE":
            EDGE = EDGE + 1
        elif row[11] == "GPRS":
            GPRS = GPRS +1
        elif row[11] == "UMTS":
            UMTS = UMTS +1
        elif row[11] == "LTE":
            LTE = LTE +1
        else:
            UNKNOWN = UNKNOWN + 1
            print("{} is Unknown".format(row[11]))
        if int(row[6]) >= -70:
            good = good + 1
        elif int(row[6]) >= -80:
            medium = medium + 1
        elif int(row[6]) >= -95:
            bad = bad + 1
        else:
            terrible = terrible +1
    data.seek(0)
    next(read)
    techtot = HSPA + HSPAP + HSDPA + NA + EDGE + GPRS + UMTS + LTE + UNKNOWN
    if HSPAP > 0:
        HSPAP = HSPAP/techtot*100
        messagetext2 = messagetext2 + "%.2f" %HSPAP + "% of data points were HSPA+\n"
    if HSPA > 0:
        HSPA = HSPA/techtot*100
        messagetext2 = messagetext2 + "%.2f" %HSPA + "% of data points were HSPA\n"
    if HSDPA > 0:
        HSDPA = HSDPA/techtot*100
        messagetext2 = messagetext2 + "%.2f" %HSDPA + "% of data points were HSDPA\n"
    if UMTS > 0:
        UMTS = UMTS/techtot*100
        messagetext2 = messagetext2 + "%.2f" %UMTS + "% of data points were UMTS\n"
    if EDGE > 0:
        EDGE = EDGE/techtot*100
        messagetext2 = messagetext2 + "%.2f" %EDGE + "% of data points were EDGE\n"
    if GPRS > 0:
        GPRS = GPRS/techtot*100
        messagetext2 = messagetext2 + "%.2f" %GPRS + "% of data points were GPRS\n"
    if LTE > 0:
        LTE = LTE/techtot*100
        messagetext2 = messagetext2 + "%.2f" %LTE + "% of data points were LTE\n"
    if UNKNOWN > 0:
        UNKNOWN = UNKNOWN/techtot*100
        messagetext2 = messagetext2 + "%.2f" %UNKNOWN + "% of datapints were UNKNOWN\n"
    if NA > 0:
        NA = NA/techtot*100
        messagetext2 = messagetext2 + "%.2f" %NA + "% of datapints were out of coverage\n"
    total = good + medium + bad + terrible
    if good > 0:
        good = good/total*100
    if bad > 0:
        bad = bad/total*100
    if medium > 0:
        medium = medium/total*100
    if terrible > 0:
        terrible = terrible/total*100
    messagetext = "%.2f" %good + "% of data points were greater than -70 dB\n"\
                  + "%.2f" %medium + "% of data points were between -80 dB and -70 dB\n" \
                  + "%.2f" %bad + "% of data points were between -80 dB and -95 dB\n" \
                  + "%.2f" %terrible + "% of datapints were worse than -95 dB \n"
    #This loop creates a list of unique CellIds contained in the CSV file
    myl = list()
    for row in read:
        if row[11] == 'LTE':
            if row[0] not in myl:
                myl.append(row[0])
        else:
            if row[11] != 'LTE':
                if row[1] not in myl:
                    myl.append(row[1])
    #this loop creates a unique style block for each unique cell id
    listlength = len(myl)
    mylint = list(map(int, myl))
    mylint = (sorted(mylint, key=int))
    for i in myl:
        try:
            ii = trans_dict[str(i)]
            myl2.append(ii)
        except:
            myl2.append(i)
    returnString = var2 + ' loaded\n\nDrive Test RSLs:\n----------------------------------------------------------------------\n' + messagetext + '\nTechnology Types seen:\n----------------------------------------------------------------------\n' + messagetext2 + '\nUnique cells seen ' + str(listlength) + ':\n----------------------------------------------------------------------\n' + fmtcols(myl2,2)
    return returnString


def colorstyle(option, prefDict):
    '''
    option - type of KML being created (CELL or RSL)
    prefDict - Preference dictionary
    Returns:
    If given CELL returns a list of colors to be used when creating the styles
    portion of the kml
    If given RSL returns a dictionary of RSL : color by dividing the RSL range
    by the number of colors
    '''
    SIGNAL_colors = prefDict['SIGNAL_colors']
    if option == 'CELL':
        #Possible colors for each sector detected
        colorlist = ['ffffff', '8b8378', '7fffd4', '458b74', '838b8b', 'eed5b7', '000000', '0000ff', 'a52a2a', '76ee00', 'ff7f24', '9a32cd', '2f4f4f', '7a7a7a', 'ffb6c1', 'ffff00', '00f5ff', 'ff7f00', '8b7355']
        return colorlist
    elif option == 'SIGNAL':
        SignalM = prefDict['SignalM_options']
        blue = Color('blue')
        yellow = Color('yellow')
        black = Color('black')
        red = Color('red')
        green = Color('green')
        num_colors = int(SIGNAL_colors)
        colorgradt1 = list(blue.range_to(Color("red"),num_colors + 1))
        colorgradt2 = list(yellow.range_to(Color("pink"),num_colors + 1))
        colorgrad = colorgradt1
        colorgrad_hex=[]
        for c in colorgrad:
            c = str(c.hex)
            c = c[1:]
            if len(c) == 3:
                c = c[0] + c[0] + c[1] + c[1] + c[2] + c[2]
            c1=c[:2]
            c2=c[2:4]
            c3=c[4:]
            c = c3 + c2 + c1
            colorgrad_hex.append(c)
        if SignalM == 'SiNR':
            rsl_range = -5 - 20
            start_rsl = -5
        elif SignalM == 'RSRP':
            rsl_range = -120 - -70
            start_rsl = -120
        elif SignalM == 'RSRQ':
            rsl_range = -15 - 0
            start_rsl = -15
        else:
            rsl_range = -105 - -60
            start_rsl = -105
        rsl_list = []
        for i in range(num_colors + 1):
            rsl_list.append(start_rsl)
            start_rsl = start_rsl - rsl_range/num_colors
        incr = 0
        rsl_dict = {}
        for i in range(num_colors + 1):
            rsl_dict[rsl_list[incr]] = colorgrad_hex[incr]
            incr += 1

        return rsl_dict


def cell_list_builder(filename):
    '''
    filename - the gmon file that is being processed.
    Returns - a list of all the unique CIDs seen in the given filename
    '''
    data = open(filename, 'r')
    read = csv.reader(data, delimiter = ';')
    myl = list()
    data.seek(0)
    next(read)
    for row in read:
        if row[11] == 'LTE':
            if row[0] not in myl:
                myl.append(row[0])
        elif row[1] not in myl:
            myl.append(row[1])
    return myl


def style_builder(xml, root, option, filename, prefDict):
    '''
    Builds the style portion of the KML
    Must be given
    xml - the minidom element <document>
    root - the minidom Documnet
    option - type of KML being created (CELL or RSL)
    filename - the gmon file that is being processed.
    Returns the minidom element <document> (xml)
    '''
    if option == 'CELL':
        loop_list = cell_list_builder(filename)
        colorlist = colorstyle(option, prefDict)
    elif option == 'SIGNAL':
        loop_list = colorstyle(option, prefDict)
    for obj in loop_list:
        #if there are unused colors in the color_list pick one at random
        if option == 'CELL':
            if colorlist:
                color = random.choice(colorlist)
                colorlist.remove(color)
            #else generate a random color
            else:
                color = "%06x" % random.randint(0, 0xFFFFFF)
            sel_style = str(obj)
            objstr = str(obj)
        elif option == 'SIGNAL':
            objstr = str(obj)
            color = str(loop_list[obj])
        color = 'ff{}'.format(color)
        Style = root.createElement('Style')
        Style.setAttribute('id', objstr)
        xml.appendChild(Style)
        LabelStyle = root.createElement('LabelStyle')
        Style.appendChild(LabelStyle)
        lcolor = root.createElement('color')
        lcolor.appendChild(root.createTextNode('00000000'))
        LabelStyle.appendChild(lcolor)
        IconStyle = root.createElement('IconStyle')
        Style.appendChild(IconStyle)
        icolor = root.createElement('color')
        icolor.appendChild(root.createTextNode(color))
        IconStyle.appendChild(icolor)
        scale = root.createElement('scale')
        scale.appendChild(root.createTextNode('0.5'))
        IconStyle.appendChild(scale)
        icon = root.createElement('Icon')
        IconStyle.appendChild(icon)
        icon_href = root.createElement('href')
        icon_href.appendChild(root.createTextNode('https://sites.google.com/site/pynetmony/home/iconrxl.png'))
        icon.appendChild(icon_href)
    return xml


def placemark_builder(filename, xml, root, option, prefDict):
    '''
    Builds the placemark portion of the KML
    xml - the minidom element <document>
    root - the minidom Document
    option - type of KML being created (CELL or SIGNAL)
    filename - the gmon file that is being processed.
    Returns the minidom element <document> (xml)
    '''
    data = open(filename, 'r')
    read = csv.reader(data, delimiter = ';')
    data.seek(0)
    next(read)
    trans_dict = transcell(prefDict['CellDpath'])
    lowest = 0
    highest = -200
    SignalM = prefDict["SignalM_options"]
    if option == 'SIGNAL':
        rsl_dict = colorstyle(option, prefDict)
    for row in read:
        sinr = rsrq = rsrp = 'N/A'
        if row[11] == 'LTE':
            isLTE = True
            cellrow = row[0]
            sinr = row[16]
            rsrq = row[9]
            rsrp = row[8]
        else:
            cellrow = row[1]
        if not isLTE and SignalM != "RSL":
            plotrow = False
        else:
            plotrow = True
        if plotrow:
            if SignalM == "RSL":
                measured_strength = row[6]
            elif SignalM == "SiNR":
                measured_strength = sinr
            elif SignalM == "RSRP":
                measured_strength = rsrp
            else:
                measured_strength = rsrq
            if cellrow in trans_dict:
                trans = trans_dict[str(cellrow)]
            else:
                trans = "Not in dictionary"
            desc_string = 'Cell ID: {}\nSector Name: {}\nTechnology: {}\nRSL: {}\nSiNR: {}\nRSRQ: {}\nRSRP: {}\nDate: {}\nTime:{}'.format(cellrow, trans, row[11], row[6], sinr, rsrq, rsrp, row[17], row[18])
            Placemark = root.createElement('Placemark')
            xml.appendChild(Placemark)
            pname = root.createElement('name')
            pname.appendChild(root.createTextNode(cellrow))
            Placemark.appendChild(pname)
            desc = root.createElement('description')
            desc.appendChild(root.createTextNode(desc_string))
            Placemark.appendChild(desc)
            styleURL = root.createElement('styleUrl')
            if option == 'CELL':
                styleURL.appendChild(root.createTextNode('#{}'.format(cellrow)))
            elif option == 'SIGNAL':
                if float(measured_strength) < lowest:
                    lowest = float(measured_strength)
                if float(measured_strength) > highest:
                    highest = float(measured_strength)
                sel_style = str(min(rsl_dict, key=lambda x:abs(x-float((measured_strength)))))
                styleURL.appendChild(root.createTextNode('#{}'.format(sel_style)))
            Placemark.appendChild(styleURL)
            Point = root.createElement('Point')
            Placemark.appendChild(Point)
            coords = root.createElement('coordinates')
            coords.appendChild(root.createTextNode('{},{}'.format(row[13], row[12])))
            Point.appendChild(coords)
    return xml


def kmlbuilder(infile, outfile, option):
    '''
    Builds the kml minidom document and writes it to file, uses style_builder
    and placemark_builder.
    option - type of KML being created (CELL or SIGNAL)
    infile - the gmon file that is being processed.
    outfile - the kml file that will be written to
    '''
    prefDict = readPrefs()
    headerstr = "<?xml version='1.0' encoding='UTF-8'?>\n<kml xmlns='http://earth.google.com/kml/2.1'>\n"
    root = minidom.Document()
    xml = root.createElement('Document')
    root.appendChild(xml)
    xml = style_builder(xml, root, option, infile, prefDict)
    xml = placemark_builder(infile, xml, root, option, prefDict)
    xml_str = xml.toprettyxml(indent="\t")
    with open(outfile, 'w') as f:
        f.write(headerstr)
        f.write(xml_str)
        f.write('</kml>')


def main():
    '''
    This supports a CLI interface using argparse.
    '''
    prefDict = readPrefs()
    Signal_option = prefDict["SignalM_options"]
    outpath = prefDict["dirPath"]
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="The GMON generated csv that you wish to process", type=str, nargs='?')
    parser.add_argument("-s", "--signal", help="generate SIGNAL map, update prefs.json in order to change the type of signal measured.", action="store_true")
    parser.add_argument("-c", "--cellid", help="generate cellid map", action="store_true")
    parser.add_argument("-b", "--both", help="generate both cellid and rsl map", action="store_true")
    parser.add_argument("-a", "--analyze", help="generate quick summary of gmon csv file", action="store_true")
    tester = 0
    rslcheck = None
    cellcheck = None
    args = parser.parse_args()

    if args.filename:
        filename = args.filename
        if args.both:
            rslcheck = True
            cellcheck = True
    else:
        filename = input("Please enter the filename of the GMON csv to be processed. (Full path required if not in same directory): ")
        checkoptions = input("[C]ell ID Map, [S]ignal Map, or [B]oth? ")
        checkoptions = checkoptions.lower()
        if checkoptions == 'b' or checkoptions == 's':
            rslcheck = True
        if checkoptions == 'b' or checkoptions == 'c':
            cellcheck = True
    friendly_filename = filename.split('/')
    friendly_filename = friendly_filename[-1]
    outpath = prefDict["dirPath"]
    if args.signal or rslcheck:
        kmlbuilder(filename, "{}/{}_{}.kml".format(outpath, friendly_filename[:-4], Signal_option), 'SIGNAL')
        print("File saved to {}/{}_{}.kml".format(outpath, friendly_filename, Signal_option))
    if args.cellid or cellcheck:
        kmlbuilder(filename, "{}/{}_Cell.kml".format(outpath, friendly_filename[:-4]), 'CELL')
        print("File saved to {}/{}_Cell.kml".format(outpath, friendly_filename))
    if args.analyze:
        summary_string = analyze(filename)
        print(summary_string)


if __name__ == '__main__':
    main()
