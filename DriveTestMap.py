    # DriveTest Map is a free tool that creates kml files from GMON generated data
    # Copyright (C) 2019  Christian Jones
    #
    # This file is part of DriveTest Map.
    #
    # DriveTest Map is free software: you can redistribute it and/or modify
    # it under the terms of the GNU General Public License as published by
    # the Free Software Foundation, either version 3 of the License, or
    # (at your option) any later version.
    #
    # DriveTest Map is distributed in the hope that it will be useful,
    # but WITHOUT ANY WARRANTY; without even the implied warranty of
    # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    # GNU General Public License for more details.
    #
    # You should have received a copy of the GNU General Public License
    # along with DriveTest Map.  If not, see <https://www.gnu.org/licenses/>.

import json
import tkinter as tk
from tkinter import ttk, filedialog

import drivetestProcess


class mainWindow(object):

    def __init__(self, master):
        self.prefDict = drivetestProcess.readPrefs()
        self.master = master
        self.master.title("Drive Test Map")
        self.menubar = tk.Menu(self.master)
        self.filemenu = tk.Menu(self.menubar, tearoff=0)
        self.filemenu.add_command(label="Open", command=self.getfile)
        self.filemenu.add_separator()
        self.filemenu.add_command(label="Preferences", command=self.prefs)
        self.filemenu.add_command(label="About", command=self.about)
        self.filemenu.add_command(label="Exit", command=self.master.quit)
        self.menubar.add_cascade(label="File", menu=self.filemenu)
        self.infobox = tk.StringVar()
        self.infobox.set('Please open a file to be processed')
        self.lbl = tk.Label(self.master, relief='sunken', anchor='n',
                            height = 30, border=2, width=58, wrap=350,
                            background='white', justify='left',
                            textvariable=self.infobox, takefocus='True',
                            state='active')
        self.CID = tk.Button(self.master, text="Cell ID Map",
                             command= lambda : self.process("CELL"))
        self.RSL = tk.Button(self.master, text="Signal Map",
                             command= lambda : self.process("SIGNAL"))
        self.ANL = tk.Button(self.master, text="Analyze",
                             command= lambda : self.process("ANALYZE"))
        self.master.config(menu=self.menubar)
        self.lbl.grid(row=1, column=0, columnspan=3, padx=10, pady=10, sticky='s')
        self.CID.grid(row=2, column=1, sticky='s')
        self.RSL.grid(row=2, column=2, sticky='s')
        self.ANL.grid(row=2, column=0, sticky='s')

    def getfile(self):
        self.filename = tk.filedialog.askopenfilename( filetypes = (("Text Files", "*.txt"),
                                                     ("All Files", "*.*")))
        if self.filename == '':
            self.infobox.set("************************\n"
                        "Please load a file FIRST\n"
                        "************************\n"
                        "   (Click File>Open)")
        else:
            var = self.filename.split('/')
            self.infobox.set(var[-1] + ' loaded\n'
                                       '***************************************\n'
                                       'Please click Analyze, Signal or Cell below\n'
                                       '***************************************')


    def process(self, option):
        self.prefDict = drivetestProcess.readPrefs()
        outlist = self.filename.split("/")
        outpath = self.prefDict["dirPath"]
        if outpath == '.':
            outpath = "/".join(outlist[:-1])
        if option == 'CELL':
            outname = outlist[-1][:-4] + '_Cell.kml'
            outfile = "{}/{}".format(outpath, outname)
            outfile = drivetestProcess.kmlbuilder(self.filename, outfile, 'CELL')
            self.infobox.set("{} \n\nsaved to \n\n {}".format(outname, outpath))
        elif option == 'SIGNAL':
            Signal_option = self.prefDict["SignalM_options"]
            outname = outlist[-1][:-4] + '_{}.kml'.format(Signal_option)
            outfile = "{}/{}".format(outpath, outname)
            outfile = drivetestProcess.kmlbuilder(self.filename, outfile, 'SIGNAL')
            self.infobox.set("{} \n\nsaved to \n\n {}".format(outname, outpath))
        elif option == 'ANALYZE':
            information = drivetestProcess.analyze(self.filename)
            self.infobox.set(information)

    def prefs(self):
        self.newWindow = tk.Toplevel(self.master)
        self.app = Preferences(self.newWindow)

    def about(self):
        self.newWindow = tk.Toplevel(self.master)
        self.app = About(self.newWindow)

class About(object):
    def __init__(self, master):
        message="Version:1.0\nCopyright (C) 2019  Christian Jones\n\n \
This program is free software: you can redistribute it and/or modify \
it under the terms of the GNU General Public License as published by \
the Free Software Foundation, either version 3 of the License, or \
(at your option) any later version. \n\n \
This program is distributed in the hope that it will be useful, \
but WITHOUT ANY WARRANTY; without even the implied warranty of \
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the \
GNU General Public License for more details.\n\n \
You should have received a copy of the GNU General Public License \
along with this program.  If not, see <https://www.gnu.org/licenses/>."
        self.master = master
        self.frame = tk.Frame(self.master)
        self.infobox = tk.StringVar()
        self.lbl = tk.Label(self.master, relief='sunken', anchor='n',
                            height = 20, border=2, width=58, wrap=350,
                            background='white', justify='left',
                            textvariable=self.infobox, takefocus='True',
                            state='active')
        self.infobox.set(message)
        self.lbl.grid(row=1, column=0, padx=10, pady=10, sticky='nsew')

class Preferences(object):
    def __init__(self, master):
        RSL_color_options = ['8', '16', '32', '64', '128', '256']
        SignalM_options = ['RSL', 'SiNR', 'RSRP', 'RSRQ']
        self.prefDict = drivetestProcess.readPrefs()
        self.master = master
        self.frame = tk.Frame(self.master)
        self.SIGNAL_colors_string = tk.StringVar()
        self.SIGNAL_colors_string.set(self.prefDict['SIGNAL_colors'])
        self.SIGNAL_colors_LBL = tk.Label(self.master, justify='left',
                                       text='# of Map Colors for Signal')
        self.SignalM_options_string = tk.StringVar()
        self.SignalM_options_string.set(self.prefDict['SignalM_options'])
        self.SignalM_options_LBL = tk.Label(self.master, justify='left',
                                       text='Meas. used for Signal Map')
        self.CellDpath_string = tk.StringVar()
        self.CellDpath_LBL = tk.Label(self.master, justify='left',
                                      text='Cell Dictionary Path')
        self.CellDpath_string.set(self.prefDict['CellDpath'])
        self.CellDpath_ebox = tk.Entry(self.master,
                                       textvariable=self.CellDpath_string, width=13)
        self.browsebutton = tk.Button(self.master, text="Browse",
                                      command= lambda : self.browse("file"))
        self.SaveDpath_LBL = tk.Label(self.master, justify='left', text='Save Path')
        self.SaveDpath_string = tk.StringVar()
        self.SaveDpath_string.set(self.prefDict['dirPath'])
        self.SaveDpath_ebox = tk.Entry(self.master,
                                       textvariable=self.SaveDpath_string, width=13)
        self.Savebrowsebutton = tk.Button(self.master,
                                          text="Browse",
                                          command= lambda : self.browse("directory"))
        self.SIGNAL_colors_pref = ttk.Combobox(self.master, values=RSL_color_options,
                                            state='readonly',
                                            textvariable=self.SIGNAL_colors_string)
        self.SignalM_options_pref = ttk.Combobox(self.master, values=SignalM_options,
                                            state='readonly',
                                            textvariable=self.SignalM_options_string)
        self.acceptButton = tk.Button(self.master, text = 'Apply',
                                      width = 25,
                                      command = self.update_preferences)
        self.cancelButton = tk.Button(self.master, text = 'Cancel',
                                      width = 25, command = self.close_windows)
        self.SIGNAL_colors_LBL.grid(row=1, column=1)
        self.SIGNAL_colors_pref.grid(row=1, column=2, sticky='w')
        self.SignalM_options_LBL.grid(row=2, column=1)
        self.SignalM_options_pref.grid(row=2, column=2, sticky='w')
        self.CellDpath_LBL.grid(row=3, column=1)
        self.CellDpath_ebox.grid(row=3, column=2, sticky='ew', columnspan=2)
        self.browsebutton.grid(row=3, column=4)
        self.SaveDpath_LBL.grid(row=4, column=1)
        self.SaveDpath_ebox.grid(row=4, column=2, sticky='ew', columnspan=2)
        self.Savebrowsebutton.grid(row=4, column=4)
        self.acceptButton.grid(row=5, column=2)
        self.cancelButton.grid(row=5, column=3)

    def browse(self, d_or_f):
        if d_or_f == 'directory':
            userinput = tk.filedialog.askdirectory()
            self.SaveDpath_string.set(userinput)
        if d_or_f == 'file':
            userinput = tk.filedialog.askopenfilename()
            self.CellDpath_string.set(userinput)

    def close_windows(self):
        self.master.destroy()

    def update_preferences(self):
        SIGNAL_colors_updated = self.SIGNAL_colors_string.get()
        SignalM_options_updated = self.SignalM_options_string.get()
        self.prefDict['SIGNAL_colors'] = SIGNAL_colors_updated
        self.prefDict['SignalM_options'] = SignalM_options_updated
        CellDpath_updated = self.CellDpath_string.get()
        self.prefDict['CellDpath'] = CellDpath_updated
        SaveDpath_updated = self.SaveDpath_string.get()
        self.prefDict['dirPath'] = SaveDpath_updated
        with open('prefs.json', 'w') as outfile:
            json.dump(self.prefDict, outfile, sort_keys=True, indent=4)
        self.close_windows()


def main():
    root = tk.Tk()
    app = mainWindow(root)
    root.mainloop()


if __name__ == '__main__':
    main()
